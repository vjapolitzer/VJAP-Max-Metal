# Max Metal 3D Printer

License
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

These parts are designed for use with [Trick Laser](http://www.tricklaser.com/)'s Max Metal frame. Photos and additional information can be found [here](https://sites.google.com/site/vjapolitzer/projects/rostock-max-metal-3d-printer) 

## Print recommendations
I printed most parts with 3 perimeters and around 50% infill. Better overkill than too flimsy! I used PETG for everything, but you could probably use PLA for most parts. ABS, PETG, or something higher temp than PLA would be preferable for the bed brackets.

## Mounting Hardware
All of the parts that are attached directly to the frame can be attached using the original 1/4-20 screws from the Rostock Max V2. The Viki LCD is attached using 4 M4x10mm screws. The electronics brackets are attached using M3x12mm screws. The spool holder uses an M8 bolt with a locknut for mounting the adapter with 608ZZ bearings. The PSU cover uses a combo socket/switch, and it mounts to the PSU using M3x10mm screws. The PSU attaches to its mounts using M4x6mm screws. The bed brackets can use the original 4-40 screws, but you need 4-40 nuts.

## NOTE
The Azteeg X5 Mini mount is only compatible with the V1.1!
